using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataAddala.Models;

namespace DataAddala.Controllers
{
    public class FootballStadiumsController : Controller
    {
        private AppDbContext _context;

        public FootballStadiumsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: FootballStadiums
        public IActionResult Index()
        {
            return View(_context.FootballStadiums.ToList());
        }

        // GET: FootballStadiums/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FootballStadium footballStadium = _context.FootballStadiums.Single(m => m.StadiumID == id);
            if (footballStadium == null)
            {
                return HttpNotFound();
            }

            return View(footballStadium);
        }

        // GET: FootballStadiums/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: FootballStadiums/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(FootballStadium footballStadium)
        {
            if (ModelState.IsValid)
            {
                _context.FootballStadiums.Add(footballStadium);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(footballStadium);
        }

        // GET: FootballStadiums/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FootballStadium footballStadium = _context.FootballStadiums.Single(m => m.StadiumID == id);
            if (footballStadium == null)
            {
                return HttpNotFound();
            }
            return View(footballStadium);
        }

        // POST: FootballStadiums/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(FootballStadium footballStadium)
        {
            if (ModelState.IsValid)
            {
                _context.Update(footballStadium);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(footballStadium);
        }

        // GET: FootballStadiums/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            FootballStadium footballStadium = _context.FootballStadiums.Single(m => m.StadiumID == id);
            if (footballStadium == null)
            {
                return HttpNotFound();
            }

            return View(footballStadium);
        }

        // POST: FootballStadiums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            FootballStadium footballStadium = _context.FootballStadiums.Single(m => m.StadiumID == id);
            _context.FootballStadiums.Remove(footballStadium);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
