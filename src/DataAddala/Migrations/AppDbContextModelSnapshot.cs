using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataAddala.Models;

namespace DataAddala.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataAddala.Models.FootballStadium", b =>
                {
                    b.Property<int>("StadiumID")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Capacity");

                    b.Property<int?>("LocationID");

                    b.Property<string>("Owner");

                    b.Property<string>("StadiumName");

                    b.Property<string>("Surface");

                    b.Property<DateTime>("YearOpened");

                    b.HasKey("StadiumID");
                });

            modelBuilder.Entity("DataAddala.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<int?>("FootballStadiumStadiumID");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataAddala.Models.Location", b =>
                {
                    b.HasOne("DataAddala.Models.FootballStadium")
                        .WithMany()
                        .HasForeignKey("FootballStadiumStadiumID");
                });
        }
    }
}
