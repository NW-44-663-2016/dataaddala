﻿using Microsoft.Data.Entity;

namespace DataAddala.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<Location> Locations { get; set; }
        public DbSet<FootballStadium> FootballStadiums { get; set; }
    }
}

