﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DataAddala.Models
{
    public class FootballStadium
    {
        [Key]
        public int StadiumID { get; set; }
        
        [Display(Name = "Stadium Name")]
        public string StadiumName { get; set; }
        [Display(Name = "Stadium Capacity")]
        public int Capacity { get; set; }
        [RegularExpression(@"/(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d/", ErrorMessage = "Invalid date")]
        [Display(Name = "Date Opened")]
        public DateTime YearOpened { get; set; }
        [Display(Name = "Owner")]
        public String Owner { get; set; }
        [Display(Name = "Surface")]
        public String Surface { get; set; }

        [ScaffoldColumn(true)]
        public int? LocationID { get; set; }

        // a list of all places the football stadiums are located

        public List<Location> stadiums { get; set; }

        public static List<FootballStadium> ReadAllFromCSV(string filepath)
        {
            List<FootballStadium> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => FootballStadium.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static FootballStadium OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            FootballStadium item = new FootballStadium();

            int i = 0;
            //item.StadiumID = Convert.ToInt32(values[i++]);
            item.StadiumName = Convert.ToString(values[i++]);
            item.Capacity = Convert.ToInt32(values[i++]);
            item.YearOpened = Convert.ToDateTime(values[i++]);
            item.Owner = Convert.ToString(values[i++]);
            item.Surface = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
